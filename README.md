# Diffing

[![CI Status](https://img.shields.io/travis/Eugene/Diffing.svg?style=flat)](https://travis-ci.org/Eugene/Diffing)
[![Version](https://img.shields.io/cocoapods/v/Diffing.svg?style=flat)](https://cocoapods.org/pods/Diffing)
[![License](https://img.shields.io/cocoapods/l/Diffing.svg?style=flat)](https://cocoapods.org/pods/Diffing)
[![Platform](https://img.shields.io/cocoapods/p/Diffing.svg?style=flat)](https://cocoapods.org/pods/Diffing)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Diffing is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Diffing'
```

## Author

Eugene, iosdeveloper.mail@gmail.com

## License

Diffing is available under the MIT license. See the LICENSE file for more info.
