//
//  Array+DiffUpdates.swift
//  Diffing
//
//  Created by Eugene Melkov on 22.04.2018.
//  Copyright © 2018 Curogram Inc. All rights reserved.
//

import Foundation

public extension Array where Element == Int {
    
    var indexSet: IndexSet {
        let ist = NSMutableIndexSet()
        self.forEach { (i) in
            ist.add(i)
        }
        return ist as IndexSet
    }
}

public extension Array where Element: Diffable {
    
    mutating func merge(with inArray: Array<Element>, isEqual: ((Element, Element) -> Bool)? = nil ) -> DiffResultType {
        let closure: ((Element, Element) -> Bool) = isEqual ?? { (_, _) in false }
        let currentDiff = Diff.diffing(oldArray: self, newArray: inArray, isEqual: closure)
        if currentDiff.changeCount == 0 {
            return currentDiff
        }
        self.removeAll()
        self.append(contentsOf: inArray)
        return currentDiff
    }
    
    mutating func populate(with inArray: Array<Element>,
                           isEqual: ((Element, Element) -> Bool)? = nil,
                           sortOrder: ((Element, Element) -> Bool)? = nil ) -> DiffResultType
    {
        var result = DiffResult<Element.Identifier>()
        var addedItems = ContiguousArray<Element.Identifier>()
        var updatedItems = ContiguousArray<Element.Identifier>()
        let updateClosure: ((Element, Element) -> Bool) = isEqual ?? { (item1, item2) in item1.diffIdentifier == item2.diffIdentifier }
        let orderClosure: ((Element, Element) -> Bool) = sortOrder ?? { (_, _) in true }
        
        if self.isEmpty {
            self.append(contentsOf: inArray)
            result.inserts = IndexSet.init(integersIn: (0 ..< inArray.count))
            return result
        }
        
        for i in 0 ..< inArray.count {
            let item = inArray[i]
            if let idx = self.firstIndex(where: { $0.diffIdentifier == item.diffIdentifier }) {
                if updateClosure(self[idx], item) {
                    self[idx] = item
                    updatedItems.append(self[idx].diffIdentifier)
                }
                continue
            }
            if let idx = self.firstIndex(where: { orderClosure($0, item) }) {
                self.insert(item, at: idx)
                addedItems.append(item.diffIdentifier)
            } else {
                self.append(item)
                addedItems.append(item.diffIdentifier)
            }
        }
        
        result.inserts = addedItems.compactMap { (itemId) -> Int? in
            return self.firstIndex(where: { $0.diffIdentifier == itemId })
            }.indexSet
        result.updates = updatedItems.compactMap { (itemId) -> Int? in
            return self.firstIndex(where: { $0.diffIdentifier == itemId })
            }.indexSet
        
        return result
    }
}
