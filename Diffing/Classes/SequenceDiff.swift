//
//  SequenceDiff.swift
//  Diffing
//
//  Created by Eugene on 1/18/19.
//  Inspired by: https://medium.com/grand-parade/computing-the-diff-of-two-arrays-in-a-functional-way-in-swift-be82a586a821
//

import Foundation

public struct SequenceDiff<Item1, Item2> {
    public let common: [(Item1, Item2)]
    public let removed: [Item1]
    public let inserted: [Item2]
    public init(common: [(Item1, Item2)] = [], removed: [Item1] = [], inserted: [Item2] = []) {
        self.common = common
        self.removed = removed
        self.inserted = inserted
    }
}

public func sequenceDiff<S1: Sequence, S2: Sequence>(_ first: S1, _ second: S2, with compare: (S1.Element, S2.Element) -> Bool) -> SequenceDiff<S1.Element, S2.Element> {
    let combinations = first.compactMap { firstElement in
        (firstElement, second.first {
            secondElement in compare(firstElement, secondElement)
        })
    }
    let common = combinations.filter { $0.1 != nil }.compactMap { ($0.0, $0.1!) }
    let removed = combinations.filter { $0.1 == nil }.compactMap { ($0.0) }
    let inserted = second.filter { secondElement in !common.contains { compare($0.0, secondElement) } }
    
    return SequenceDiff(common: common, removed: removed, inserted: inserted)
}
